import {useState} from 'react';
import {projects} from './data/Projects';
import TimeBar from './components/TimeBar';
import BudgetBar from './components/BudgetBar';
import ProjectInfos from './components/ProjectInfos';
import Actions from './components/Actions';
import AppelProjet from './components/AppelProjet';
import Event from './components/Event';
import Help from './components/Help';
import WinLose from './components/WinLose';

import './App.css';

function App() {
  const [days, setDays] = useState(60);
  const [project, setProject] = useState(getRandomProject());
  const [appel, setAppel] = useState(null);
  const [event, setEvent] = useState(null);
  const [help, setHelp] = useState(true);
  const [winlose, setWinlose] = useState(null);

  const passDays = (n) => {
    if(project.budget >= project.cost)
      setWinlose(true);
    if((days - n)<= 1)
      setWinlose(false);
    setDays(days - n);
  }

  return <>
    {
      winlose !== null && <WinLose win={winlose}/>
    }
    {
      event !== null && <Event close={()=>setEvent(null)} event={event} />
    }
    {
      appel !== null && <AppelProjet days={days} passDays={passDays} project={project} setProject={setProject} close={()=>setAppel(null)} appel={appel} setEvent={setEvent} />
    }
    {
      help !== null && <Help close={()=>setHelp(null)} help={help} />
    }
    <TimeBar days={days} setAppel={setAppel} />
    <div className='app__flex-content'>
      <BudgetBar budget={project.budget} cost={project.cost} />
      <ProjectInfos project={project}/>
      <Actions project={project} passDays={passDays} setProject={setProject} setEvent={setEvent} />
    </div>
  </>
}

const getRandomProject = () => {
  const max = projects.length - 1;
  return projects[Math.round(Math.random()*max)];
}

export default App;
