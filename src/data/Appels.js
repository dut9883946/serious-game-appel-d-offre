export const APPELS = [
  {
    name: 'Erasmus',
    logo: '/icons/erasmus.png',
    deadline: 40,
    minPartners: 1,
    locationRange: 0,
    budget: 3000000,
    folderLanguage: null,
    funding: .4
  },
  {
    name: 'Interreg',
    logo: '/icons/interreg.png',
    deadline: 5,
    minPartners: 2,
    locationRange: 3,
    budget: 2000000,
    folderLanguage: null,
    funding: .6
  },
  {
    name: 'Europe créative',
    logo: '/icons/europe-creative.png',
    deadline: 30,
    minPartners: 3,
    locationRange: 1,
    budget: 169000000,
    folderLanguage: 'Anglais',
    funding: .8
  },
  {
    name: 'Erasmus + jeunesse',
    logo: '/icons/erasmus-plus.png',
    deadline: 17,
    minPartners: 3,
    locationRange: 0,
    budget: 3000000,
    folderLanguage: null,
    funding: .4
  }
]