export const projects = [
  {
    name: "Développement de l'énergie géothermique",
    description: "Ce projet vise à développer l'énergie géothermique en Alsace afin de palier le manque suite à l'arrêt de la centrale nucléaire de Fessenheim",
    cost: 150000,
    budget: 25000,
    organisation: "GeoNergy",
    location: "Mulhouse",
    potiential_partners: [
      {
        name: "Alsace Energie",
        trust: 0.95
      },
      {
        name: "Centre de recherche énergétique",
        trust: 0.87
      },
      {
        name: "EuropaGeo",
        trust: 0.71
      },
      {
        name: "GeoCluster",
        trust: 0.79
      },
      {
        name: "Université de Mulhouse",
        trust: 0.98
      },
      {
        name: "Préfecture du Bas-Rhin",
        trust: 0.90
      },
    ],
    partners: [],
    englishFolder: false,
    territorialImpact: 1
  },
  {
    name: "Young Europ'Olympik",
    description: "Rencontre entre les étudiants Européens au cours d'un tournoi olympique",
    cost: 200000,
    budget: 65000,
    organisation: "Conseil Sportif Européen",
    location: "Strasbourg",
    potiential_partners: [
      {
        name: "Complexe sportif de Stuttgart",
        trust: 0.88
      },
      {
        name: "Complexe sportif de Londres",
        trust: 0.84
      },
      {
        name: "Jeunes Sportifs",
        trust: 0.90
      },
      {
        name: "Idadis",
        trust: 0.78
      },
      {
        name: "Université de Strasbourg",
        trust: 0.99
      },
      {
        name: "Préfecture du Bas-Rhin",
        trust: 0.98
      },
    ],
    partners: [],
    englishFolder: false,
    territorialImpact: 3
  },
  {
    name: "AllTogether",
    description: "Mise en place d'un lieu de rencontre entre étudiants natifs et étrangers afin de mieux les intégrer autour de la culture et des passions de chacun",
    cost: 12500,
    budget: 3500,
    organisation: "UnPourTous",
    location: "Paris",
    potiential_partners: [
      {
        name: "ErasmusExchange",
        trust: 0.86
      },
      {
        name: "Association bénévole de l'université",
        trust: 0.98
      },
      {
        name: "BDE de l'université",
        trust: 0.97
      },
      {
        name: "Université de Paris",
        trust: 0.87
      },
      {
        name: "Préfecture de Paris",
        trust: 0.84
      },
    ],
    partners: [],
    englishFolder: false,
    territorialImpact: 0
  },
  {
    name: "SymphonyNight",
    description: "Concert rassemblant des jeunes musiciens talentueux autour de la culture bretonne",
    cost: 25000,
    budget: 10000,
    organisation: "Music'Ally",
    location: "Rennes",
    potiential_partners: [
      {
        name: "Bretagne Music",
        trust: 0.95
      },
      {
        name: "Mairie de Rennes",
        trust: 0.92
      },
      {
        name: "Beethoven Store",
        trust: 0.97
      },
      {
        name: "Université de Rennes",
        trust: 0.91
      },
      {
        name: "Préfecture de Rennes",
        trust: 0.88
      },
    ],
    partners: [],
    englishFolder: false,
    territorialImpact: 1
  },
  {
    name: "RobHealth",
    description: "Développement d'un robot destiné à l'aide au personnes agés dans les hôpitaux",
    cost: 400000,
    budget: 143000,
    organisation: "ModernTech&Comp",
    location: "Paris",
    potiential_partners: [
      {
        name: "CPA Paris",
        trust: 0.96
      },
      {
        name: "XiaoMed",
        trust: 0.85
      },
      {
        name: "AI-Tec",
        trust: 0.81
      },
      {
        name: "Pascal Elec",
        trust: 0.96
      },
      {
        name: "HealthEngine",
        trust: 0.91
      },
      {
        name: "MedicalBinary",
        trust: 0.90
      },
      {
        name: "Recherche et Développpement IA",
        trust: 0.98
      },
      {
        name: "Hôpital de Paris",
        trust: 0.99
      },
      {
        name: "Université de Paris",
        trust: 0.87
      },
      {
        name: "Préfecture de Paris",
        trust: 0.82
      },
    ],
    partners: [],
    englishFolder: false,
    territorialImpact: 3
  },
  {
    name: "JeunesCréatifs",
    description: "Aidez les jeunes artistes à se faire connaître et se créer des contacts dans le milieu",
    cost: 60000,
    budget: 27000,
    organisation: "PaintAcademy",
    location: "Marseille",
    potiential_partners: [
      {
        name: "La France a du talent",
        trust: 0.93
      },
      {
        name: "Radio Marseille",
        trust: 0.97
      },
      {
        name: "Beaux-Arts",
        trust: 0.95
      },
      {
        name: "Artistes Français",
        trust: 0.96
      },
      {
        name: "Université de Marseille",
        trust: 0.98
      },
      {
        name: "Préfecture de Marseille",
        trust: 0.97
      },
    ],
    partners: [],
    englishFolder: false,
    territorialImpact: 2
  },
  {
    name: "ElectroBus",
    description: "Mise en place de nouveaux bus électrique afin de remplacer ceux à essence qui permettrait a terme d'économiser énormement d'argent",
    cost: 500000,
    budget: 255000,
    organisation: "RouleZen",
    location: "Strasbourg",
    potiential_partners: [
      {
        name: "EcoBus",
        trust: 0.99
      },
      {
        name: "FlexiFlexo",
        trust: 0.93
      },
      {
        name: "SNCF",
        trust: 0.92
      },
      {
        name: "CTS",
        trust: 0.98
      },
      {
        name: "Université de Strasbourg",
        trust: 0.86
      },
      {
        name: "Préfecture du Bas-Rhin",
        trust: 0.94
      },
    ],
    partners: [],
    englishFolder: false,
    territorialImpact: 0
  },
  {
    name: "MixCitizen",
    description: "Fortification des liens entre la France/Allemagne/Suisse grâce à des journées immersives dans des milieux professionnels voisins pour les jeunes étudiants",
    cost: 123000,
    budget: 64500,
    organisation: "Préfecture du Haut et Bas-Rhin",
    location: "Strasbourg",
    potiential_partners: [
      {
        name: "SECO",
        trust: 0.93
      },
      {
        name: "Canton de Bâle",
        trust: 0.92
      },
      {
        name: "BadenBadenLand",
        trust: 0.99
      },
      {
        name: "Préfecture du Bas-Rhin",
        trust: 0.95
      },
      {
        name: "Conférence du Rhin supérieur",
        trust: 0.98
      },
    ],
    partners: [],
    englishFolder: false,
    territorialImpact: 3
  },
]

export const cofunding = [
  {
    name: "Banque locale",
    contribution: 0.25
  },
  {
    name: "Institut Européenne",
    contribution: 0.4
  },
  {
    name: "La région",
    contribution: 0.3
  },
  {
    name: "Collectivité locale",
    contribution: 0.35
  },
  {
    name: "Une association locale",
    contribution: 0.1
  },
  {
    name: "La municipalité locale",
    contribution: 0.15
  },
  {
    name: "Une entreprise privée",
    contribution: 0.2
  }
]
