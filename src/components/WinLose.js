import './css/WinLose.css';

const WinLose = (props) => {
  return <>
    <div className={`win-lose ${props.win ? 'win' : 'lose'}`}>
      {getResultat(props)}
      <div className="note">
        {
          props.win ? 
            "Vous avez réussi à financer votre projet dans les temps." :
            "Vous n'avez pas réussi à financer votre projet dans les temps."
        }
      </div>
      <div className="button" onClick={()=>window.location.reload()}>
        <svg xmlns="http://www.w3.org/2000/svg" width="36" height="36" fill="currentColor" viewBox="0 0 16 16">
          <path fill-rule="evenodd" d="M8 3a5 5 0 1 1-4.546 2.914.5.5 0 0 0-.908-.417A6 6 0 1 0 8 2v1z"/>
          <path d="M8 4.466V.534a.25.25 0 0 0-.41-.192L5.23 2.308a.25.25 0 0 0 0 .384l2.36 1.966A.25.25 0 0 0 8 4.466z"/>
        </svg>
      </div>
    </div>
  </>
}

const getResultat = (props) => {
  if(props.win){
    return <>
      <div className="win-title">Félicitations !</div>
    </>
  }
  else{
    return <>
      <div className="lose-title">Dommage !</div>
    </>
  }

}

export default WinLose;