import React from 'react';
import Bubble from './Bubble';
import {APPELS} from '../data/Appels';

import './css/TimeBar.css';

const TimeBar = (props) => {
  return <>
    <section className='time-bar'>
      <div className='time-bar__container'>
        <div className='time-bar__value' style={{width: ((props.days > 0 ? props.days : 0)/60)*99+'%'}}>
          <div className='time-bar__remaining-days'>
            <div className='time-bar__days-divider'></div>
            {props.days > 0 ? props.days : 0} jour(s) restant(s)
          </div>
        </div>
      </div>
      
      <div className='time-bar__cursor'>
        <Bubble right={'calc('+(((APPELS[0].deadline/60)*70)+15)+'% - 57px)'} >
          <img 
            className={`time-bar__cursor-logo ${(APPELS[0].deadline+1)>props.days && 'disabled'}`}
            alt='' src={process.env.PUBLIC_URL+'/icons/erasmus.png'}
            onClick={()=>{if(APPELS[0].deadline<props.days)props.setAppel(APPELS[0])}}>
          </img>
        </Bubble>
      </div>
      <div className='time-bar__cursor'>
        <Bubble right={'calc('+(((APPELS[3].deadline/60)*70)+15)+'% - 46.5px)'} >
          <img
            className={`time-bar__cursor-logo ${(APPELS[3].deadline+1)>props.days && 'disabled'}`}
            alt='' src={process.env.PUBLIC_URL+'/icons/erasmus-plus.png'}
            onClick={()=>{if(APPELS[3].deadline<props.days)props.setAppel(APPELS[3])}}></img>
        </Bubble>
      </div>
      <div className='time-bar__cursor'>
        <Bubble right={'calc('+(((APPELS[1].deadline/60)*70)+15)+'% - 87px)'}>
          <img
            className={`time-bar__cursor-logo ${(APPELS[1].deadline+1)>props.days && 'disabled'}`}
            alt='' src={process.env.PUBLIC_URL+'/icons/interreg.png'}
            onClick={()=>{if(APPELS[1].deadline<props.days)props.setAppel(APPELS[1])}}>
          </img>
        </Bubble>
      </div>
      <div className='time-bar__cursor'>
        <Bubble right={'calc('+(((APPELS[2].deadline/60)*70)+15)+'% - 65px)'}>
          <img
            className={`time-bar__cursor-logo ${(APPELS[2].deadline+1)>props.days && 'disabled'}`}
            alt='' src={process.env.PUBLIC_URL+'/icons/europe-creative.png'}
            onClick={()=>{if(APPELS[2].deadline<props.days)props.setAppel(APPELS[2])}}>
          </img>
        </Bubble>
      </div>

    </section>
  </>
}

export default TimeBar;