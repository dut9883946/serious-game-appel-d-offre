import './css/Event.css';

const Event = (props) => {
  return <>
    <div className='event'>
      <div className='event__buttonClose'>
        <svg onClick={ props.close } xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="currentColor" class="bi bi-x" viewBox="0 0 16 16">
          <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
        </svg>
      </div>
      <div className='event__info'>
        <div className='event__title'>
          { props.event[0] }
        </div>
        <div className='event__description'>  
          { props.event[1] }
        </div>
      </div>
    </div>
  </>
} 

export default Event;