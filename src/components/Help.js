import './css/Help.css';

const Help = (props) => {
    return <>
    <div className='help'>
        <div className='help__buttonClose'>
            <svg onClick={ props.close } xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="currentColor" class="bi bi-x" viewBox="0 0 16 16">
                <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
            </svg>
        </div>
        <div className='help__info'>
            <div className='help__title'>
                Aide
            </div>
            <div className='help__description'>
                Bienvenue dans ProjectStar !<br/><br/>
                Aujourd'hui vous allez devenir porteur de projet. <br/>
                Tentez de financer le projet qui vous est imposé grâce aux appels d'offres et aux actions disponibles.
                <br/>Attention à respecter les délais ! 
                <p>Bonne chance !</p>
            </div>
        </div>
    </div>
    </>
}

export default Help;