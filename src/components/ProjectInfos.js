import './css/ProjectInfos.css';

const ProjectInfos = (props) => {
  return <>
  <div className='project-infos'>
    <div className="project-infos__content">   
      <div className="project-infos__title">Description du projet</div>
      <div className="project-infos__top">
        <div className="project-infos__organisation">
          <svg style={{marginRight: 6, transform: 'translateY(2px)'}} xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-bookmarks" viewBox="0 0 16 16">
            <path d="M2 4a2 2 0 0 1 2-2h6a2 2 0 0 1 2 2v11.5a.5.5 0 0 1-.777.416L7 13.101l-4.223 2.815A.5.5 0 0 1 2 15.5V4zm2-1a1 1 0 0 0-1 1v10.566l3.723-2.482a.5.5 0 0 1 .554 0L11 14.566V4a1 1 0 0 0-1-1H4z"/>
            <path d="M4.268 1H12a1 1 0 0 1 1 1v11.768l.223.148A.5.5 0 0 0 14 13.5V2a2 2 0 0 0-2-2H6a2 2 0 0 0-1.732 1z"/>
          </svg>
          { props.project.organisation }
        </div>
        <div className="project-infos__location">
          <svg style={{marginRight: 6, transform: 'translateY(2px)'}} xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-geo-alt" viewBox="0 0 16 16">
            <path d="M12.166 8.94c-.524 1.062-1.234 2.12-1.96 3.07A31.493 31.493 0 0 1 8 14.58a31.481 31.481 0 0 1-2.206-2.57c-.726-.95-1.436-2.008-1.96-3.07C3.304 7.867 3 6.862 3 6a5 5 0 0 1 10 0c0 .862-.305 1.867-.834 2.94zM8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10z"/>
            <path d="M8 8a2 2 0 1 1 0-4 2 2 0 0 1 0 4zm0 1a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
          </svg>
          { props.project.location }
        </div>
      </div>   
      <div className="project-infos__name">
        { props.project.name }
      </div>
      <div className="project-infos__description">
        { props.project.description }
      </div>
      <div className="project-infos__cost">
        <div className="project-infos__cost-text">
          Coût du projet : 
        </div>
        <div className="project-infos__cost-text"> 
          { props.project.cost } €
        </div>
      </div>
      <div className="project-infos__territorialImpact">
        <div className="project-infos__territorialImpact-text">
          Impact : 
        </div>
        <div className="project-infos__territorialImpact-text"> 
          { getImpact(props.project) }
        </div>
      </div>
      <div className="project-infos__language">
        <div className="project-infos__language-text">
          Langue du dossier : 
        </div>
        <div className="project-infos__language-text"> 
          { props.project.englishFolder ? 'Anglais' : 'Français' }
        </div>
      </div>
      <div className="project-infos__text-partners">
        {getParters(props.project.partners)}
      </div>
      <div className="project-infos__list-partners">
        {
          props.project.partners.map(partner=>(
            <>
              <div className="project-infos__partners">
                <div className="project-infos__partners-name">
                  {partner.name}
                </div>
                <div className="project-infos__partners-trust">
                  Confiance {partner.trust*100}%
                </div>
              </div>
            </>
          ))
       }
      </div>
    </div>
  </div>
  </>
}

const getParters = (parteners) => {
  if(parteners.length===1){
    return <>
      <span>Partenaire :</span>
    </>
  }
  if(parteners.length>1){
    return <>
      <span>Partenaires :</span>
    </>
  }
}

const getImpact = (project) => {
  var res;
  if(project.territorialImpact===0){
    res="Départemental";
  }
  else if(project.territorialImpact===1){
    res="Régional";
  }
  else if(project.territorialImpact===2){
    res="National";
  }
  else{
    res="International";
  }
  return res;
}

export default ProjectInfos;