import './css/AppelProjet.css';

const AppelProjet = (props) => {
  return <>
    <div className='appel-projet'>
      <div className='appel-projet__top'>
        <img className='appel-projet__logo' alt='' src={process.env.PUBLIC_URL+props.appel.logo} ></img>
        <svg className='appel-projet__buttonClose' onClick={ props.close } xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="currentColor" viewBox="0 0 16 16">
          <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
        </svg>
      </div>
      <div className='appel-projet__name'>
        { props.appel.name }
      </div>

      <div className=''>
        <div className='appel-projet__partners'>
          <div className='appel-projet__partners-text'>      
            Nombres partenaires minimum :
          </div>
          <div className='appel-projet__partners-number'>
            { props.appel.minPartners }
          </div>  
        </div>
        <div className='appel-projet__budget'>
          <div className='appel-projet__budget-text'>      
            Budget total :
          </div>
          <div className='appel-projet__budget-number'>
            { props.appel.budget } €
          </div> 
        </div>
        <div className='appel-projet__englishfolder'>
          <div className='appel-projet__englishfolder-text'>      
            Langage du dossier :
          </div>
          <div className='appel-projet__englishfolder-number'>
          {getEnglish(props.appel.folderLanguage)}
          </div>    
        </div>
        <div className='appel-projet__location'>
          <div className='appel-projet__location-text'>
          Echelle minimum du projet : 
          </div>
          <div className='appel-projet__location-number'>
          {getScaleProject(props.appel.locationRange)}
          </div>    
        </div>
      
      
      </div>
      <div className='appel-projet__candidater' onClick={()=>candidater(props)}>
        Canditater à cette appel
        <div className='appel-projet__candidater-deadline'>
          {props.days-props.appel.deadline} jours
        </div>
      </div>
    </div>
  </>
}

const candidater = (props) => {
  var score = 0;
  if(props.project.partners.length >= props.appel.minPartners)
    score+=.4;
  if(props.appel.folderLanguage === null || props.project.englishFolder)
    score+=.1;
  if(props.appel.locationRange >= props.project.territorialImpact)
    score+= .1;
  if(Math.random() < score) {
    const res = props.project;
    const amount = Math.round(props.appel.funding * props.project.cost);
    res.budget += amount;
    props.passDays(props.days-props.appel.deadline);
    props.setEvent(["Félicitations !","Vous avez remporté l'appel d'offre : "+props.appel.name+"\n Cela ajoute "+amount+" € à votre budget !"]);
  }
  else{
    props.passDays(props.days-props.appel.deadline);
    props.setEvent(["Dommage !","Vous n'avez pas remporté l'appel d'offre, essayez d'améliorer votre dossier !"]);
  }
  props.close();
}



const getEnglish = (language) => {
  if(language===null){
    return <>
      Peu importe
    </>
  }
  else{
    return <>
      {language}
    </>
  }
}

const getScaleProject = (location) => {
  var res;
  if(location===3){
    res="International";
  }
  else if(location===2){
    res="National";
  }
  else if(location===1){
    res="Regional"
  }
  else{
    res="Départemental";
  }
  return res;
}

export default AppelProjet;