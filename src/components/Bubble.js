import './css/Bubble.css';

const Bubble = (props) => {
  return <>
  <div className='bubble'>
    <div className='bubble__content' style={{right: props.right}}>
      {props.children}
      <img alt='' className='bubble__bottom' src={process.env.PUBLIC_URL+'/icons/bubbleThing.svg'}></img>
    </div>
  </div>
  </>
}

export default Bubble;