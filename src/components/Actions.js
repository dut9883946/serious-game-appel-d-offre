import './css/Actions.css';
import {cofunding} from '../data/Projects';

const Actions = (props) => {
  return <>
    <div className='actions'>
      <div className='actions__title'>Actions</div>
      <div className='actions__content'>
        {
          ACTIONS.map(action => (
            action.enabled(props) &&
            <div className='actions__action' onClick={()=>action.apply(props)}>
              <div className='actions__action-name'>{action.name}</div>
              <div className='actions__action-cost'>
                {action.cost} jours
              </div>
            </div>
          ))
        }
      </div>
    </div>
  </>
}

// const verifAbandonPartner = (props) => {
//   for(var i=0;i<props.project.partners.length;i++)
//     if(Math.random() > props.project.partners[i].trust){
//       var res = {...props.project};
//       const msg=["Mauvaise nouvelle !", props.project.partners[i].name+" vous à lachement abandonné !"];
//       res.partners.splice(i,1);
//       props.setProject(res);  
//       return msg;
//     }
// }

const ACTIONS = [
  {
    name: 'Chercher un partenaire',
    enabled: ()=>true,
    cost: 5,
    apply: (props) => {
      props.passDays(5);
      if(Math.random() > .5){
        var res = {...props.project};
        const index = Math.round(Math.random()*(props.project.potiential_partners.length-1));
        const newPartner = props.project.potiential_partners[index];
        props.project.potiential_partners.splice(index,1);
        res.partners.push(newPartner);
        props.setProject(res);
        props.setEvent(["Félicitations !",newPartner.name+" a accepté votre demande de partenariat ! "]);
      }
      else{
        props.setEvent(["Mauvaise nouvelle !","Aucun partenaire n'a accepter votre demande de partenariat ! Perseverez !"]);
      }
    }
  },
  {
    name: 'Chercher un cofinancement',
    enabled: ()=>true,
    cost: 7,
    apply: (props) => {
      if(Math.random() > .7){
        var res = {...props.project};
        const index = Math.round(Math.random()*(cofunding.length-1));
        const newCoFunding = cofunding[index];
        res.budget += Math.round(newCoFunding.contribution * res.cost);
        props.setProject(res);
        props.passDays(7);
        props.setEvent(["Félicitations !",newCoFunding.name+" a accepté votre demande de co-financement ! Vous bénéficiez de "+Math.round(newCoFunding.contribution*res.cost)+" €"]);
      }
      else{
        props.passDays(7);
        props.setEvent(["Mauvaise nouvelle !","Aucun co-financeur n'a accepter votre demande de co-financement !"])
      }
    }
    
  },
  {
    name: 'Rédiger son dossier en anglais',
    enabled: (props) => !props.project.englishFolder,
    cost: 4,
    apply: (props) => {
      props.passDays(4);
      const res = {...props.project};
      res.englishFolder = true;
      props.setProject(res);
    }
  },
  {
    name: 'Travailler la communication',
    enabled: (props)=>props.project.territorialImpact!==3,
    cost: 3,
    apply: (props) => {
      props.passDays(3);
      if(Math.random() > .66){
        const res = {...props.project};
        res.territorialImpact++;
        props.setProject(res);
        props.setEvent(["Félicitations ! Votre projet à pris de l'ampleur !"])
      }
    }
  }

]

export default Actions;