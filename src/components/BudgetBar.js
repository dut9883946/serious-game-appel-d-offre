import './css/BudgetBar.css';
import kichta from './img/kichta.png';

const BudgetBar = (props) => {
  return <>
    <div className='budget-bar'>
      <img className='budget-bar__kichta' src={kichta} alt="kichta"></img>
      <div className='budget-bar__container'>
        <div className='budget-bar__value' style={{height: props.budget/props.cost*100+'%'}}>
          <div className='budget-bar__number'>
            {props.budget} €
          </div>
        </div>
      </div>
    </div>
  </>
}

export default BudgetBar;