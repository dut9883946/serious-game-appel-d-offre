# THE BOYS - T4 Project

## Somaire ##
>   * [Présentation du jeu](#présentation) 
>   * [Installation et execution](#Installation-et-exécusion) 
>   * [Captures d'écran](#captures-d'écran)  
>   * [Cahier des charges](#cahier-des-charges) 
>   * [Outils utilisés ](#outils-utilisés)
>   * [Groupe](#groupe)
>   * [Tuteur](#tuteur)

## Présentation ##

### Objectif ###

Project Star est un jeu dis "serious game" où le but est de remporter un appel d'offre pour avoir le budget nécéssaire pour réaliser votre project.

### Déroulement d'une partie ###

Vous êtes porteur d'un projet qui vous est imposé, votre objectif est de trouver les financements nécessaires à sa réalisation. Pour ce faire, vous devez effectuer des actions qui vont augmenter vos chances de remporter des appels d'offres. Attention, votre temps est compté, chaque action vous coûte du temps, il vous faudra candidater à temps aux appels d'offres avant les deadlines et réussir à financer votre projet dans les délais !

## Installation et exécusion ##
Une seule étape :
- Rendez-vous sur se lien <a href="https://project-star-game.web.app/">https://project-star-game.web.app/</a> avec votre navigateur

Attention il faudra peut être activer javascript dans votre navigateur si ce dernier ne l'est pas !

## Captures d'écran ##
Notion de temps :
![alt text](./captureEcran/temps.png)

Informations de votre projet :
![alt text](./captureEcran/project.png)

Action à réaliser pour améliorer le projet :
![alt text](./captureEcran/actions.png)

Informations des appels d'offres :
![alt text](./captureEcran/appelOffre.png)
## Cahier des charges ##
Voici notre [cahier des charges en .md](https://git.unistra.fr/the-boys/the-boys-deluxe/mel21-t4/-/blob/master/cdc.md).
Voici notre [cahier des charges en drive](https://docs.google.com/document/d/1YwVey17eBDarAefUoCGjntVQNixn519Dkt2mRj2ovVg/edit?usp=sharing).
## Outils utilisés ##
Nous avons dévellopé notre jeu avec le framework  [React](https://fr.reactjs.org/). 
## Groupe ##
- Bouvier Lilian <<lbouvier@etu.unistra.fr>>
- Burckel Luc <<luc.burckel@etu.unistra.fr>>
- Nguyen Toan <<toan.nguyen@etu.unistra.fr>>

## Tuteur ##
- Meuley Loic <<loic.meuley@etu.unistra.fr>>