#
## Document de travail T4

Projet : Project Star

- Rédacteurs : Bouvier Lilian, Burckel Luc, Nguyen Toan
- URL GIT : https://git.unistra.fr/the-boys/the-boys-deluxe/mel21-t4

Instructions :

1. Prendre des notes, identifier des questions et obtenir des réponses lors de la rencontre tuteur.trice.
2. Synthétiser les notes la section « Description des objectifs pédagogiques du jeu »
3. Réaliser un prototype d&#39;un jeu sérieux à partir de ces notes.
4. Compléter les autres sections de ce document de travail
5. Copier [https://gitlab.unistra.fr/gossa/t432/-/blob/master/Evaluation-T4.md](https://gitlab.unistra.fr/gossa/t432/-/blob/master/Evaluation-T4.md) sur votre dépôt GIT.
6. Accompagner les étudiants en T2 remplir cette évaluation puis à améliorer ce document en utilisant la fonction &quot;commentaire&quot;
7. Transformer ce document finalisé, sauf les notes de rencontre, en README.md de votre projet avec todo-list (&quot;- []&quot;) pour chaque item.

## Description des objectifs pédagogiques du jeu

## **Objectif pédagogique général**

Le fait de remporter un appel d&#39;offre peut être un facteur important dans la réussite et l&#39;aboutissement d&#39;un projet culturel ou d&#39;innovation. C&#39;est pourquoi il est important pour les porteurs de ces projets de bien comprendre les enjeux des appels.

## **Description des objectifs pédagogiques**

### **Projet**

- Les projets ont des envergures différentes : départemental à international.
- Ils ont également un coût qui dépend de l&#39;ampleur du projet.
- Les projets réunissent des partenaires ayant des ambitions similaires et ainsi que des co-financeurs pour épauler ces derniers.

### **Réalisé des actions**

- Afin de satisfaire les critères des différents appels d&#39;offres, le joueur est amené à effectuer diverses actions.
- Il devra chercher des partenaires ayant un projet similaire au sien.
- Il devra également chercher des co-financeur pour apporter un soutien financier au projet
- Les actions prennent du temps à être réalisé, il faudra donc faire chaque action consciencieusement.

### **Partenaires**

- Augmente la chance de remporter des appels d&#39;offres
- il peut commencer à vous accompagner ou vous abandonner au fil des jours.

### **Trouver des co financeurs**

- Permet d&#39;apporter un soutien financier pour le projet.

### **Gestion du temps**

- Une deadline générale est imposée au joueur.
- Certains appels d&#39;offres ont une deadline avant d&#39;autres ce qui amène le joueur à faire attention au choix de ses actions selon l&#39;appel d&#39;offre.

##

## Description du jeu

- **Type de jeu** : Gestion
- **Incarnation du joueur** : Porteur d&#39;un projet

## **Déroulement d&#39;une partie**

La partie commence avec un projet aléatoire assigné au joueur, il va devoir trouver les financements nécessaires à la réalisation de celui-ci. Pour ce faire, il va devoir effectuer des actions comme trouver des partenaires ou encore des cofinancements, puis participer à des appels d&#39;offres.

Le temps est compté et chaque action coûte un certain nombre de jours.

La partie se termine lorsque le joueur arrive à la fin du temps imparti et n&#39;a pas réussi à rassembler la somme requise pour son projet, ou quand le budget du projet atteint le seuil désiré.

## **Paramétrage d&#39;une partie**

Description des options permettant de paramétrer une partie.

- Coût du projet : la somme requise pour réaliser le projet
- Budget : la somme de départ de la partie
- Echelle territoriale : l&#39;ampleur de l&#39;impact territorial du projet
- Langue du dossier : la langue dans laquelle est rédigé le dossier
- Nombre de jours : nombre de jours disponible
- Deadlines des appels : Les dates limites pour candidater aux différents appels d&#39;offre

## Modèle conceptuel applicatif

Liste, MCD ou diagramme de classe décrivant le jeu, et en particulier les entités, en séparant ce qui est exposé au joueur de ce qui est interne au jeu.

- Project :
  - nom : le nom du projet
  - description : la description du projet
  - entreprise et lieu
  - coût : le coût du projet
  - impact : impact au niveau territorial
  - dossier : langue
  - Partenaire:
    - Nom : le nom du partenaire
    - Confiance : pourcentage de confiance de votre partenaire

- Appel d&#39;offre :
  - nom : le nom
  - deadline : deadline pour y participer
  - partenaire minimum : Nombre de partenaire minimum pour accéder à l&#39;appel d&#39;offre avec votre projet
  - budget total : Budget total disponible pour cette appel d&#39;offre
  - dossier : langage du dossier nécessaire
  - Échelle : échelle minimum nécessaire

- Action : une tâche réalisée pour améliorer le projet
  - tâche : la tâche réalisé par l&#39;action
  - succès : probabilité de succès que l&#39;action fonction
  - jour : jour pour l&#39;effectuer

## Description des fonctionnalités

## **Entrée**

Liste des actions possibles par le joueur.

###  **Gestion des tours** 

-  **Choix**  : Choix d&#39;une action et passages de jours

 **Actions** 

-  **Chercher un partenaire** 
-  **Chercher un co-financeur** 
-  **Rédiger son dossier en anglais** 
-  **Travailler la communication** 

## **Sorties**

Liste des informations présentées au joueurs. Peut être organisé en module.

### **Barre d&#39;informations**

- **Jour(s) restant(s)** : nombre de jours disponible avant la fin
-  **Budget actuel**  : somme possédée actuellement
-  **Appels d&#39;offres :**  choix sur les différents appels

### **Projet**

-  **Nom**  : nom du projet
-  **Organisation**  : nom de l&#39;organisation à l&#39;origine du projet

-  **Localisation**  : localisation de cette organisation
-  **Description**  : description du projet
-  **Coût**  : coût du projet
-  **Langue**  : langue du dossier
-  **Partenaires**  : partenaires avec lesquels vous avez fait un partenariat
-  **Impact**  : impact au niveau territorial

## **Moteur interne**

Liste des interactions entre entrées et sorties

###  **Gestion des tours** 

- temps restant = temps restant - durée d&#39;action

###  **Gestion des tâches** 

- Réalisation de l&#39;action

## Scénarios

## **Scénario tutorial**

Décrire en détail un scénario qui s&#39;appuie sur toutes les fonctionnalités mais sans difficulté pour le joueur.

 **Déroulement gagnant :** 

1.
2.
3.

 **Déroulement perdant :** 

1. Candidater à l&#39;appel d&#39;offre Interreg
2. Chercher un cofinancement

## **Scénarios complémentaires**

Décrire moins précisément des idées de scénarios.

1.
2.

## **Fonctionnalités additionnelles**

Décrire ici les idées de fonctionnalités additionnelles. Cette partie ne doit servir qu&#39;en dernier recours, pour transmettre ce qui n&#39;a pas été inclus dans les fonctionnalités faute de temps.

- Ajustez les deadlines de chaque appel d&#39;offre à chaque nouvelle partie selon le projet